import json
import re

'''
little note to myself about how did I get the json with images list:

copy pasted source of:
https://www.reddit.com/r/image_linker_bot/comments/2znbrg/image_suggestion_thread_20/
to a file named 'tmp'

replaced \|(.*)\|(.*)\| with "$1": "$2",
removed \[[0-9]\]
removed parenthesis ()
replaced gifv with gif
removed last line comma
added json parenthesis {} to wrap everything
executed this simple script
'''

with open('tmp') as f:
    j = json.load(f)

res = {}

for k in j:
    res[k] = j[k].split(' ')

with open('image_list.json', 'w') as f:
    json.dump(res, f)
