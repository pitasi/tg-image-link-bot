import telebot
import json
import re
import random
from config import *

# Creating the bot
bot = telebot.TeleBot(config['tg_token'])
bot_info = bot.get_me()
# Opening image list file and slicing aliases
with open(config['image_file']) as f: tmp = json.load(f)
image_map = {}
triggers_list = []
for k in tmp:
    triggers_list.append('> ' + k + '\n')
    for name in k.split(','):
        image_map[name.strip()] = tmp[k]
tmp = None
# This is the magic string used to show an image without downloading it
inline_mode = u'*{}*[\u2063]({})'
normal_mode = u'bleep bloop...*{}*[\u2063]({})'

def get_hashtags(m):
    if not m.entities or len(m.entities) == 0: return []
    res = []
    for i in m.entities:
        if i.type == 'hashtag':
            res.append(m.text[i.offset+1 : i.offset+i.length].lower())
    return res

def get_image(image_name):
    try:
        return random.choice(image_map[image_name])
    except KeyError as e:
        print('Image not found: {}'.format(image_name))
        return None

def send_image(m, image_name):
    image_url = get_image(image_name)
    if image_url:
        # If message is replying to someone, reply the image here.
        # Otherwise reply to who asked for it.
        bot.reply_to(m.reply_to_message if m.reply_to_message else m,
                     normal_mode.format(image_name, image_url),
                     parse_mode='markdown')

# Handle inline queries
@bot.inline_handler(lambda q: q.query)
def query_text(q):
    image = get_image(q.query.strip().lower().replace('#',''))
    if not image:
        bot.answer_inline_query(q.id, [])
        return

    text = telebot.types.InputTextMessageContent(inline_mode.format(q.query, image), parse_mode='markdown')
    r = telebot.types.InlineQueryResultArticle('1', 'Click here to send.', text, description=q.query, hide_url=True)
    bot.answer_inline_query(q.id, [r])

# Handling /start and /help
@bot.message_handler(commands=['start', 'help'])
def help_handler(m):
    if m.chat.type == 'group' or m.chat.type == 'supergroup':
        bot.reply_to(m, 'We should talk in private to avoid flood in here.')
        return
    bot.send_message(m.chat.id,
        "Hi.\nTo use me, you can add me in your group and use hashtags (#ayylmao), or just write in the message box my tag (@{}) followed by the trigger.".format(bot_info.username))

    tmp = '<b>Here\'s the full list of triggers:</b>\n'
    tmp_list = triggers_list

    while len(tmp_list) != 0:
        while len(tmp) < 2000 and len(tmp_list) != 0:
            tmp += tmp_list.pop(0)
        bot.send_message(m.chat.id, tmp, parse_mode='html')
        tmp = ''

# Handling all the text messages
@bot.message_handler(func=lambda m: m.entities)
def main_handler(m):
    images = get_hashtags(m)

    # Slicing tuples to avoid spam
    for i in (images[:config['max_per_message']] if config['max_per_message'] > 0 else images):
        send_image(m, i)

print('@{} started.'.format(bot_info.username))
bot.polling()
